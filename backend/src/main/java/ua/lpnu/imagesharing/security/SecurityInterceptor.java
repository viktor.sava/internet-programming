package ua.lpnu.imagesharing.security;

import lombok.RequiredArgsConstructor;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import ua.lpnu.imagesharing.controller.Secured;
import ua.lpnu.imagesharing.controller.exception.NoPermissionException;
import ua.lpnu.imagesharing.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
public class SecurityInterceptor implements HandlerInterceptor {

    private final UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            if (handlerMethod.getMethod()
                    .isAnnotationPresent(Secured.class)) {
                String email = request.getHeader("email");
                if (email != null && userService.hasUserPermission(email)) {
                    return true;
                } else {
                    throw new NoPermissionException(String.format("User with email %s have no permission", email));
                }
            }
        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}
