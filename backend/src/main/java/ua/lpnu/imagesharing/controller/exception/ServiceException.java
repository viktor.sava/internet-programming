package ua.lpnu.imagesharing.controller.exception;

public abstract class ServiceException extends RuntimeException {
    public ServiceException(String message) {
        super(message);
    }
}
