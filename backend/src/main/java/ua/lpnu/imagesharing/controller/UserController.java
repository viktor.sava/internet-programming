package ua.lpnu.imagesharing.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.imagesharing.controller.dto.UserDto;
import ua.lpnu.imagesharing.service.UserService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@CrossOrigin("http://localhost:3000")
public class UserController {

    private final UserService userService;

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping
    public UserDto createUser(@RequestBody @Validated UserDto userDto) {
        return userService.createUser(userDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{email}")
    public UserDto getUser(@PathVariable String email) {
        return userService.getUser(email);
    }

}
