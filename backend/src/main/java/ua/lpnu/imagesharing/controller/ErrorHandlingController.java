package ua.lpnu.imagesharing.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ua.lpnu.imagesharing.controller.dto.response.ErrorResponse;
import ua.lpnu.imagesharing.controller.exception.ServiceException;

import javax.servlet.ServletException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
@Log4j2
public class ErrorHandlingController {

    @ExceptionHandler({ServiceException.class, ServletException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<ErrorResponse> handleServiceServletException(Exception ex) {
        log.info("handleServiceServletException", ex);
        return List.of(new ErrorResponse(HttpStatus.BAD_REQUEST, ex.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public List<ErrorResponse> handleException(Exception ex) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        ex.printStackTrace(printWriter);
        log.error("Unsupported exception", ex);
        return List.of(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), stringWriter.toString()));
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<ErrorResponse> handleBindException(BindException ex) {
        return ex.getBindingResult()
                .getAllErrors()
                .stream()
                .map(m -> new ErrorResponse(HttpStatus.BAD_REQUEST, m.getDefaultMessage()))
                .collect(Collectors.toList());
    }

}
