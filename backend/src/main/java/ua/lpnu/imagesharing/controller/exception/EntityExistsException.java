package ua.lpnu.imagesharing.controller.exception;

public class EntityExistsException extends ServiceException {
    public EntityExistsException(String message) {
        super(message);
    }
}
