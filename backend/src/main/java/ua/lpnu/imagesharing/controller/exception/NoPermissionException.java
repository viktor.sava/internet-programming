package ua.lpnu.imagesharing.controller.exception;

public class NoPermissionException extends ServiceException {
    public NoPermissionException(String message) {
        super(message);
    }
}
