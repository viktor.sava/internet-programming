package ua.lpnu.imagesharing.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.lpnu.imagesharing.controller.dto.ImageDto;
import ua.lpnu.imagesharing.service.ImageService;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/image")
@CrossOrigin("http://localhost:3000")
public class ImageController {

    private final ImageService imageService;

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("{id}")
    @Secured
    public ImageDto uploadImage(@RequestParam("file") MultipartFile multipartFile,
                                @PathVariable Long id) throws IOException {
        return imageService.uploadImage(multipartFile, id);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping
    @Secured
    public ImageDto saveImage(@RequestBody ImageDto imageDto) {
        return imageService.saveImage(imageDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("{id}")
    @Secured
    public void deleteImage(@PathVariable Long id) {
        imageService.deleteImage(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getImage(@PathVariable Long id) throws IOException {
        return imageService.getImage(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/tags")
    public List<ImageDto> getImagesByTags(@RequestBody Set<String> tags) {
        return imageService.getImagesByTags(tags);
    }

}
