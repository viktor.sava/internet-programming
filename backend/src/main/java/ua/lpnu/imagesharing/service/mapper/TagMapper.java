package ua.lpnu.imagesharing.service.mapper;

import org.mapstruct.Mapper;
import ua.lpnu.imagesharing.controller.dto.TagDto;
import ua.lpnu.imagesharing.service.model.Tag;

@Mapper(componentModel = "spring")
public interface TagMapper {
    TagDto tagToDto(Tag tag);

    Tag dtoToTag(TagDto tagDto);

    default String dtoToString(TagDto tagDto) {
        return tagDto.getName();
    }

    default TagDto stringToDto(String name) {
        return new TagDto(name);
    }
}
