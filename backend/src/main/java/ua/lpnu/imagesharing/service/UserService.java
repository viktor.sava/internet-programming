package ua.lpnu.imagesharing.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.lpnu.imagesharing.controller.dto.UserDto;
import ua.lpnu.imagesharing.controller.exception.EntityExistsException;
import ua.lpnu.imagesharing.controller.exception.EntityNotFoundException;
import ua.lpnu.imagesharing.service.mapper.UserMapper;
import ua.lpnu.imagesharing.service.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    public boolean hasUserPermission(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(String.format("User with email %s is not found", email)))
                .getIsAdmin();
    }

    public UserDto createUser(UserDto userDto) {
        if (userRepository.existsByEmail(userDto.getEmail())) {
            throw new EntityExistsException(String.format("User with email %s already exists", userDto.getEmail()));
        }
        return userMapper.userToDto(userRepository.save(userMapper.dtoToUser(userDto)));
    }

    public UserDto getUser(String email) {
        return userMapper.userToDto(userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityExistsException(String.format("User with email %s is not found", email))));
    }
}
