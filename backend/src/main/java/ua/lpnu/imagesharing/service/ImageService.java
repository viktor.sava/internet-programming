package ua.lpnu.imagesharing.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ua.lpnu.imagesharing.controller.dto.ImageDto;
import ua.lpnu.imagesharing.controller.dto.TagDto;
import ua.lpnu.imagesharing.controller.exception.EntityExistsException;
import ua.lpnu.imagesharing.controller.exception.EntityNotFoundException;
import ua.lpnu.imagesharing.service.mapper.ImageMapper;
import ua.lpnu.imagesharing.service.model.Image;
import ua.lpnu.imagesharing.service.repository.ImageContentStore;
import ua.lpnu.imagesharing.service.repository.ImageRepository;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ImageService {

    private final ImageRepository imageRepository;

    private final ImageContentStore imageContentStore;

    private final ImageMapper imageMapper;

    private final TagService tagService;

    public List<ImageDto> getImagesByTags(Set<String> tags) {
        tags.removeIf(String::isBlank);
        if (tags.size() == 0) {
            return imageRepository.findAll()
                    .stream()
                    .map(imageMapper::imageToDto)
                    .collect(Collectors.toList());
        }
        return imageRepository.findDistinctByTagsIn(new HashSet<>(tagService.getTags(tags.stream()
                        .map(TagDto::new)
                        .collect(Collectors.toSet()))))
                .stream()
                .map(imageMapper::imageToDto)
                .collect(Collectors.toList());
    }

    public ImageDto uploadImage(MultipartFile multipartFile, Long id) throws IOException {
        Image image = imageRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Image with id %d is not found", id)));
        if (image.getContentId() == null) {
            image.setContentMimeType(multipartFile.getContentType());
            imageContentStore.setContent(image, multipartFile.getInputStream());
            return imageMapper.imageToDto(imageRepository.save(image));
        } else {
            throw new EntityExistsException("Image is already uploaded");
        }
    }

    public ImageDto saveImage(ImageDto imageDto) {
        Image entity = imageMapper.dtoToImage(imageDto);
        entity.setTags(new HashSet<>(tagService.getOrSaveTags(imageDto.getTags()
                .stream()
                .map(TagDto::new)
                .collect(Collectors.toSet()))));
        return imageMapper.imageToDto(imageRepository.save(entity));
    }

    public void deleteImage(Long id) {
        if (!imageRepository.existsById(id)) {
            throw new EntityNotFoundException(String.format("Image with id %d is not found", id));
        }
        imageRepository.deleteById(id);
    }

    public byte[] getImage(Long id) throws IOException {
        Image image = imageRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Image with id %d is not found", id)));
        return IOUtils.toByteArray(imageContentStore.getContent(image));
    }

}
