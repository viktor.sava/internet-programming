package ua.lpnu.imagesharing.service.repository;

import org.springframework.content.commons.repository.ContentStore;
import org.springframework.content.rest.StoreRestResource;
import ua.lpnu.imagesharing.service.model.Image;

@StoreRestResource
public interface ImageContentStore extends ContentStore<Image, String> {
}
