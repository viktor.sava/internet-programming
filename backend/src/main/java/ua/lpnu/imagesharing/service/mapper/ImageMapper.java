package ua.lpnu.imagesharing.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ua.lpnu.imagesharing.controller.dto.ImageDto;
import ua.lpnu.imagesharing.service.model.Image;

@Mapper(uses = TagMapper.class, componentModel = "spring")
public interface ImageMapper {
    @Mapping(target = "url", source = "contentId")
    ImageDto imageToDto(Image image);

    Image dtoToImage(ImageDto imageDto);

}
