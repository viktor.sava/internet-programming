package ua.lpnu.imagesharing.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ua.lpnu.imagesharing.service.model.Image;
import ua.lpnu.imagesharing.service.model.Tag;

import java.util.List;
import java.util.Set;

@RepositoryRestResource(path="files", collectionResourceRel="files")
@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    List<Image> findDistinctByTagsIn(Set<Tag> tags);
}
