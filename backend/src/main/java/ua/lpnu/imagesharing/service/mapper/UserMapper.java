package ua.lpnu.imagesharing.service.mapper;

import org.mapstruct.Mapper;
import ua.lpnu.imagesharing.controller.dto.UserDto;
import ua.lpnu.imagesharing.service.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User dtoToUser(UserDto userDto);

    UserDto userToDto(User user);
}
