package ua.lpnu.imagesharing.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import ua.lpnu.imagesharing.controller.dto.TagDto;
import ua.lpnu.imagesharing.service.mapper.TagMapper;
import ua.lpnu.imagesharing.service.model.Tag;
import ua.lpnu.imagesharing.service.repository.TagRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log4j2
public class TagService {

    private final TagRepository tagRepository;

    private final TagMapper tagMapper;

    public List<Tag> getTags(Set<TagDto> tags) {
        return tagRepository.findAllByNameIn(tags.stream()
                .map(TagDto::getName)
                .collect(Collectors.toSet()));
    }

    public List<Tag> getOrSaveTags(Set<TagDto> tags) {
        List<Tag> foundTags = getTags(tags);
        if (foundTags.size() != tags.size()) {
            List<Tag> newTags = tags.stream()
                    .filter(tagDto -> foundTags.stream()
                            .noneMatch(tag -> tagDto.getName()
                                    .equals(tag.getName())))
                    .map(tagMapper::dtoToTag)
                    .collect(Collectors.toList());
            tagRepository.saveAll(newTags);
            foundTags.addAll(newTags);
        }
        return foundTags;
    }
}
