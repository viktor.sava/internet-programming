import React from 'react';
import { Button, Container, Form, FormCheck, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import { myConfig } from './config';

export default class AuthForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = { email: "", isAdmin: false, errors: [], user: {} };
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.register = this.register.bind(this);
        this.handleIsAdminChange = this.handleIsAdminChange.bind(this);

        localStorage.setItem('user', JSON.stringify({email: "empty", is_admin: false}));
    }

    login(event) {
        this.setState({errors: []});
        fetch(myConfig.apiUrl + '/user/' + this.state.email, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (!response.ok) {
                throw response.json();
            }
            return response.json(); 
        }).then(data => {
            localStorage.setItem('user', JSON.stringify(data));
            this.setState({user: data});
            this.props.updateUser(data);
        }).catch(errorHandler => {
            errorHandler.then((data) => {
                this.setState({errors: data})
            });
            this.setState({user: {}});
        });
    }

    register(event) {
        this.setState({errors: []});
        fetch(myConfig.apiUrl + '/user', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'email': this.state.email,
                'is_admin': this.state.isAdmin == 'on' ? true : false
            })
        }).then(response => {
            if (!response.ok) {
                throw response.json();
            }
            return response.json(); 
        }).then(data => {
            localStorage.setItem('user', JSON.stringify(data));
            this.setState({user: data});
            this.props.updateUser(data);
        }).catch(errorHandler => {
            errorHandler.then((data) => {
                this.setState({errors: data})
            });
            this.setState({user: {}});
        });
    }

    logout(event) {
        this.setState({errors: []});
        this.setState({user: {}});
        localStorage.setItem('user', JSON.stringify({email: "empty", is_admin: false}));
        this.props.updateUser({email: "empty", is_admin: false});
    }

    handleIsAdminChange(event) {
        this.setState({isAdmin: event.target.value});
    }

    handleEmailChange(event) {
        this.setState({email: event.target.value});
    }

    render() {
        let status = "";
        let errors = "";
        const isAuth = Object.keys(this.state.user).length;
        if (isAuth) {
            status = <h4> User id {this.state.user.id} - {this.state.user.is_admin ? 'admin' : 'user'}</h4>;
        }
        if (this.state.errors.length != 0) {
            this.state.errors.forEach((element) => errors = <h6 className='m-1'>{element.message}</h6>);
        }
        return (
            <Container className='m-1'>
                <h3>Auth</h3>
                {status}
                {errors}
                <Form>
                    <FormGroup className='mb-3'>
                        <FormLabel>Email</FormLabel>
                        <FormControl disabled={isAuth} placeholder='email' name='email' value={this.state.email} onChange={this.handleEmailChange}/>
                    </FormGroup>
                    <FormGroup className='mb-3'>
                        <FormCheck disabled={isAuth} label='is admin?' onClick={this.handleIsAdminChange}/>
                    </FormGroup>
                    <Button disabled={isAuth} className='m-1' onClick={this.login}>Login</Button>
                    <Button disabled={isAuth} className='m-1' onClick={this.register}>Register</Button>
                    <Button disabled={!isAuth} className='m-1' onClick={this.logout}>Logout</Button>
                </Form>
            </Container>
        );
    }
}  