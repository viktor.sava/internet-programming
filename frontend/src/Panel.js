import React, { useEffect } from 'react'
import { Container, Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import { myConfig } from './config';
import UserContext from './UserContext';

export default class Panel extends React.Component {

    static contextType = UserContext;

    componentDidMount() {
        const user = this.context;

        console.log(user);
    }

    constructor(props) {
        super(props);

        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleTagsChange = this.handleTagsChange.bind(this);
        this.handleIdChange = this.handleIdChange.bind(this);
        this.delete = this.delete.bind(this);
        this.upload = this.upload.bind(this);

        this.state = {
            tags: [],
            file: null,
            id: 0,
            errors: []
        };
    }

    delete(event) {
        this.setState({errors: []});
        fetch(myConfig.apiUrl + '/image/' + this.state.id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'email': JSON.parse(localStorage.getItem('user')).email
            },
            body: JSON.stringify({
                id: this.state.id
            })
        }).then(response => {
            if (!response.ok) {
                throw response.json();
            }
            return response.json();
        }).catch(errorHandler => {
            errorHandler.then((data) => {
                this.setState({errors: data})
            });
        });
    }

    upload(event) {
        this.setState({errors: []});
        fetch(myConfig.apiUrl + '/image', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'email': JSON.parse(localStorage.getItem('user')).email
            },
            body: JSON.stringify({
                tags: this.state.tags
            })
        }).then(response => {
            if (!response.ok) {
                throw response.json();
            }
            return response.json(); 
        }).then(data => {
            var formData = new FormData();
            formData.append('file', this.state.file);
            fetch(myConfig.apiUrl + '/image/' + data.id, {
                method: 'PUT',
                headers: {
                    'email': JSON.parse(localStorage.getItem('user')).email
                },
                body: formData
            }).then(response2 => {
                if (!response2.ok) {
                    throw response2.json();
                }
                return response2.json();
            }).catch(errorHandler2 => {
                errorHandler2.then((data2) => {
                    this.setState({errors: data2})
                });
            });
        }).catch(errorHandler => {
            errorHandler.then((data) => {
                this.setState({errors: data})
            });
        });
    }

    handleTagsChange(event) {
        this.setState({tags: event.target.value.split(" ")})
    }

    handleFileChange(event) {
        this.setState({file: event.target.files[0]});
    }

    handleIdChange(event) {
        this.setState({id: event.target.value});
    }

    render() {
        let errors = "";
        if (this.state.errors.length != 0) {
            this.state.errors.forEach((element) => errors = <h6 className='m-1'>{element.message}</h6>);
        }
        return (
            <Container className='m-1'>
                <h3>Panel</h3>
                {errors}
                <Form>
                    <h5>Add</h5>
                    <FormGroup className='m-1'>
                        <FormLabel>Tags</FormLabel>
                        <FormControl onChange={this.handleTagsChange}></FormControl>
                    </FormGroup>
                    <FormGroup className='m-1'>
                        <FormLabel>Choose the file of image</FormLabel>
                        <FormControl onChange={this.handleFileChange} type='file'></FormControl>
                    </FormGroup>
                    <Button className='m-1' onClick={this.upload}>Upload</Button>
                </Form>
                <Form>
                    <h5>Remove</h5>
                    <FormGroup className='m-1'>
                        <FormLabel>Enter image id</FormLabel>
                        <FormControl onChange={this.handleIdChange}></FormControl>
                    </FormGroup>
                    <Button className='m-1' onClick={this.delete}>Remove</Button>
                </Form>
            </Container>
        );
    }
}