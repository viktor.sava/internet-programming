import './App.css';
import React, { createContext } from 'react';
import AuthForm from './AuthForm';
import Panel from './Panel';
import Search from './Search';
import { Col, Container, Row } from 'react-bootstrap';


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {is_admin: false};
    this.update = this.update.bind(this);
  }

  update(user) {
    this.setState({is_admin: user.is_admin});
  }

  render() {
    return (
      <Container className="App justify-content-center">
        <Row>
          <Col><AuthForm updateUser={this.update}/></Col>
          <Col hidden={!this.state.is_admin}><Panel/></Col>
        </Row>
        <Row>
          <Search></Search>
        </Row>
      </Container>
    );
  }
}