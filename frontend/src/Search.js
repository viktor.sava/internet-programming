import React from "react"
import { Container, Form, FormControl, Button, FormLabel, Card, CardImg } from "react-bootstrap";
import CardHeader from "react-bootstrap/esm/CardHeader";
import { myConfig } from "./config";

export default class Search extends React.Component {
    constructor(props) {
        super(props);

        this.handleSearchInput = this.handleSearchInput.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.delete = this.delete.bind(this);

        this.state = {
            tags: [],
            images: []
        };
    }

    handleSearchInput(event) {
        this.setState({tags: event.target.value.split(" ")})
    }

    delete(event) {
        fetch(myConfig.apiUrl + '/image/' + event.target.getAttribute("img_id"), {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'email': JSON.parse(localStorage.getItem('user')).email
            },
            body: JSON.stringify({
                id: event.target.getAttribute("img_id")
            })
        }).then(response => {
            // if (!response.ok) {
            //     throw response.json();
            // }
            return response.json();
        }).catch(errorHandler => {
            // errorHandler.then((data) => {
            //     this.setState({errors: data})
            // });
        });
    }

    onSubmit(event) {
        fetch(myConfig.apiUrl + '/image/tags', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.tags)
        })
        .then(response => {
            if (!response.ok) {
                throw response.json();
            }
            return response.json(); 
        }).then(data => {
            this.setState({images: data});
        });
    }

    render() {
        return (
            <Container className='m-1'>
                <Form>
                    <FormControl className='mb-3' placeholder="tags..." onChange={this.handleSearchInput}></FormControl>
                    <Button onClick={this.onSubmit}>Search</Button>
                </Form>
                { this.state.images.map(({id, tags}) => 
                <>
                <Card key={id}>
                    <CardHeader >Tags - [{tags.join(', ')}], id - {id}</CardHeader>
                    <img className="mb-3" style={{maxWidth: '100%'}} src={myConfig.apiUrl + '/image/' + id} />
                    <Button hidden={JSON.parse(localStorage.getItem('user')).is_admin === false} onClick={this.delete} img_id={id}>Delete</Button>
                </Card>
                {/* <h5 className='mb-3'>Tags - [{tags.join(', ')}], id - {id}</h5><img className="mb-3" style={{maxWidth: '100%'}} src={myConfig.apiUrl + '/image/' + id} key={id} /> */}
                </>) }
            </Container>
        );
    }
}